package Step;

import org.openqa.selenium.By;		
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.Given;		
import cucumber.api.java.en.Then;		
import cucumber.api.java.en.When;		

public class PaySuccess {
	WebDriver driver;
	@Given("^Open the Chrome and launch the application$")				
    public void chormeOpen() throws Throwable							
    {		
        //System.out.println("This Step open the chrome and launch the application.");
		 //System.setProperty("webdriver.gecko.driver", "E://Selenium//Selenium_Jars//geckodriver.exe");					
		System.setProperty("webdriver.chrome.driver","..//SeleniumTest//browser//chromedriver.exe");    
		driver =new ChromeDriver();				
	    driver.manage().window().maximize();			
	    driver.get("https://demo.midtrans.com/");
    }		

    @When("^I Enter Correct Payment Credit Card$")					
    public void enterCorrectPaymentCreditCard() throws Throwable 							
    {		
      driver.findElement(By.linkText("BUY NOW")).click();
      {
        WebElement element = driver.findElement(By.linkText("BUY NOW"));
        Actions builder = new Actions(driver);
        builder.moveToElement(element).perform();
      }
      {
        WebElement element = driver.findElement(By.tagName("body"));
        Actions builder = new Actions(driver);
        builder.moveToElement(element, 0, 0).perform();
      }
    	driver.findElement(By.cssSelector(".cart-checkout")).click();
        driver.switchTo().frame(0);
        Thread.sleep(1000);
        driver.findElement(By.cssSelector(".button-main-content")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector(".with-promo .list-caption")).click();
        driver.findElement(By.name("cardnumber")).click();
        driver.findElement(By.name("cardnumber")).sendKeys("4811 1111 1111 1114");
        driver.findElement(By.cssSelector(".col-xs-7 > input")).click();
        driver.findElement(By.cssSelector(".col-xs-7 > input")).sendKeys("02 / 20");
        driver.findElement(By.cssSelector(".col-xs-5 > input")).click();
        driver.findElement(By.cssSelector(".col-xs-5 > input")).sendKeys("123");
        driver.findElement(By.cssSelector(".button-main-content")).click();
        driver.switchTo().frame(0);
        Thread.sleep(1000);
        driver.findElement(By.id("PaRes")).click();
        driver.findElement(By.id("PaRes")).sendKeys("112233");
        driver.findElement(By.name("ok")).click();
        
        
    					
    }		

    @Then("^Success Credit Card Payment$")					
    public void successCreditCardPayment() throws Throwable 							
    {   
//    	Thread.sleep(2000);
//    	driver.switchTo().defaultContent();
//        {
//          WebElement element = driver.findElement(By.cssSelector(".button-main-content"));
//          Actions builder = new Actions(driver);
//          builder.moveToElement(element).clickAndHold().perform();
//        }
//        {
//          WebElement element = driver.findElement(By.cssSelector(".button-main-content"));
//          Actions builder = new Actions(driver);
//          builder.moveToElement(element).perform();
//        }
//        {
//          WebElement element = driver.findElement(By.cssSelector(".button-main-content"));
//          Actions builder = new Actions(driver);
//          builder.moveToElement(element).release().perform();
//        }
//        driver.findElement(By.cssSelector(".button-main-content")).click();
        System.out.println("Payment Berhasil.");    
        driver.close();
    }		

}
